import Vue from 'vue';
import VueRouter from 'vue-router';
import Index from '../views/Index.vue';
import NoteItem from '../views/NoteItem.vue';
import NotFound from '../views/NotFound'
Vue.use(VueRouter);
const routes = [{
    path: '/',
    name: 'home',
    component: Index
  },
  {
    path: '/note/:id',
    name: 'noteItem',
    component: NoteItem,
    props: true
  },
  { 
    path: '/404', 
    name: '404', 
    component: NotFound, 
  },

  {
    path: '*',
    redirect: '/404'
  }
];

const router = new VueRouter({
  routes, // short for `routes: routes`,
});

export default router;