import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);


let getStateDefaults = function () {
  return JSON.parse(localStorage.getItem('initial_state'))
}

const store = new Vuex.Store({
  state: getStateDefaults(),
  getters: {
    getNotes: (state) => state.notes,
    getNotesWithTodos: (state) => Object.values(state.notes).map(note => ({
      ...note,
      todos: state.todos[note.id]
    })),
    getTodos: (state) => state.todos,
    getNoteById: (state) => (noteId) => state.notes[noteId] || null,
    getNoteTodos: (state) => (noteId) => state.todos[noteId] || [],
  },
  mutations: {
    EMPTY_STATE(state, note) {
        let {
          todos,
          ...noteWithoutTodo
        } = note;
        Vue.set(state.notes, note.id, noteWithoutTodo);
        Vue.set(state.todos, note.id, todos);
        localStorage.setItem('initial_state', JSON.stringify(state));
    },

    SYNC_NOTE(state, note) {
      let {
        todos,
        ...noteWithoutTodo
      } = note;

      Vue.set(state.notes, note.id, noteWithoutTodo);
      Vue.set(state.todos, note.id, todos);
      localStorage.setItem('initial_state', JSON.stringify(state));
    },
    DELETE_NOTE(state, noteId) {
      Vue.delete(state.notes, noteId);
      Vue.delete(state.todos, noteId);
    },
    SYNC_TODO(state, noteId, todo) {
      let oldTodos = state.todos[noteId];
      let todoIndex = oldTodos.findIndex((item) => item.id === todo.id);
      if (todoIndex < 0) {
        oldTodos.splice(todoIndex, 1, todo);
        Vue.set(state.todos, noteId, oldTodos);
      } else {
        oldTodos.push(todo);
        Vue.set(state.todos, noteId, oldTodos);
      }
      localStorage.setItem('initial_state', JSON.stringify(state));
    },
    DELETE_TODO(state, noteId, todoId) {
      let oldTodos = state.todos[noteId];
      let todoIndex = oldTodos.findIndex((item) => item.id === todoId);
      oldTodos.splice(todoIndex, 1);
      Vue.set(state.todos, noteId, oldTodos);
    },
  },
});

export default store;