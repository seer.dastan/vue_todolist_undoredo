import { cloneDeep } from "lodash";
const EMPTY_STATE = 'EMPTY_STATE';
export default {
    install(Vue, options = {}) {
        if (!Vue._installedPlugins.find(plugin => plugin.Store)) {
            throw new Error("VuexUndoRedo plugin must be installed after the Vuex plugin.")
        }
        Vue.mixin({
            data() {
                return {
                    done: [],
                    undone: [],
                    newMutation: true,
                    ignoreMutations: options.ignoreMutations || []
                };
            },
            created() {
                if (this.$store) {
                    this.$store.subscribe(mutation => {
                        if (mutation.type !== EMPTY_STATE && this.ignoreMutations.indexOf(mutation.type) === -1) {
                            this.done.push(cloneDeep(mutation));
                        }
                        if (this.newMutation) {
                            this.undone = [];
                        }
                    });
                }
            },
            computed: {
                canRedo() {
                    return this.undone.length;
                },
                canUndo() {
                    return this.done.length;
                }
            },
            methods: {
                redo() {
                    // let commit = this.undone.pop();
                    this.newMutation = false;

                    if(this.undone.length > 0) {
                        let mutation = this.undone[this.undone.length -1];
                        this.$store.commit(mutation.type, Object.assign({}, mutation.payload));
                    }

                    this.newMutation = true;
                    this.undone.pop();
                },
                undo() {
                    this.undone.push(this.done.pop());
                    this.newMutation = false;
                    var mutationPayload = this.done.length > 0 ? this.done[this.done.length -1].payload : this.initialState;
                    this.$store.commit(EMPTY_STATE, mutationPayload);
                    this.newMutation = true;
                }
            }
        });
    },
}