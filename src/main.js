import '@babel/polyfill';
import 'mutationobserver-shim';
import 'bootstrap'
import Vue from 'vue';

import './plugins/bootstrap-vue';
import VuexUndoRedo from './plugins/vuex-undo-redo';
import router from './router';
import store from './store';
import App from './App.vue';

Vue.config.productionTip = false;
Vue.use(VuexUndoRedo);

localStorage.setItem('initial_state', JSON.stringify({
  notes: {
    _0dzgys1z7: {
      id: '_0dzgys1z7',
      title: 'Купить лицуху Windows 10'
    }
  },
  todos: {
    _0dzgys1z7: [{
        id: '_3szbnk95s',
        completed: false,
        text: 'Найти где дешевле'
      },
      {
        id: '_gue47x4py',
        completed: false,
        text: 'Купить'
      }
    ]
  },
}));

new Vue({
  render: (h) => h(App),
  router,
  store,
}).$mount('#app');